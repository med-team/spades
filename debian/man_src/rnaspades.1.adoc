= rnaspades(1)

## NAME

rnaspades - SPAdes genome assembler (rnaSPAdes mode)

## SYNOPSIS

*rnaspades* [options] -o <output_dir>

## DESCRIPTION

*rnaspades* runs **SPAdes** in rnaSPAdes mode.

## OPTIONS

### Basic options

*-o* <output_dir>::
   directory to store all the resulting files (required)

*--iontorrent*::
   this flag is required for IonTorrent data

*--test*::
   runs SPAdes on toy dataset

*-h*/*--help*::
   prints this usage message

### Input data

*--12* <filename>::
   file with interlaced forward and reverse paired-end reads

*-1* <filename>::
   file with forward paired-end reads

*-2* <filename>::
   file with reverse paired-end reads

*-s* <filename>::
   file with unpaired reads

*--pe<#>-12* <filename>::
   file with interlaced reads for paired-end library number <#> (<#> = 1,2,3,4,5)

*--pe<#>-1* <filename>::
  file with forward reads for paired-end library number <#> (<#> = 1,2,3,4,5)
*--pe<#>-2* <filename>::
  file with reverse reads for paired-end library number <#> (<#> = 1,2,3,4,5)
*--pe<#>-s* <filename>::
  file with unpaired reads for paired-end library number <#> (<#> = 1,2,3,4,5)
*--pe<#>-<or>*::
   orientation of reads for paired-end library number <#> (<#> = 1,2,3,4,5; <or> = fr, rf, ff)
*--s<#>* <filename>::
  file with unpaired reads for single reads library number <#> (<#> = 1,2,3,4,5)
*--trusted-contigs* <filename>::
  file with trusted contigs
*--untrusted-contigs* <filename>::
   file with untrusted contigs

### Pipeline options

*--only-error-correction*::
    runs only read error correction (without assembling)
*--only-assembler*::
     runs only assembling (without read error correction)
*--continue*::
     continue run from the last available check-point
*--restart-from* <cp>::
     restart run with updated options and from the specified check-point ('ec', 'as', 'k<int>', 'mc')
*--disable-gzip-output*::
    forces error correction not to compress the corrected reads
*--disable-rr*::
      disables repeat resolution stage of assembling

### Advanced options

*--dataset* <filename>::
   file with dataset description in YAML format
*-t*/*--threads* <int>::
    number of threads [default: 16]
*-m*/*--memory* <int>::
    RAM limit for SPAdes in Gb (terminates if exceeded) [default: 250]
*--tmp-dir* <dirname>::
    directory for temporary files [default: <output_dir>/tmp]
*-k* <int,int,...>::
    comma-separated list of k-mer sizes (must be odd and less than 128) [default: 'auto']
*--cov-cutoff* <float>::
    coverage cutoff value (a positive float number, or 'auto', or 'off') [default: 'off']
*--phred-offset* <33 or 64>::
    PHRED quality offset in the input reads (33 or 64) [default: auto-detect]
